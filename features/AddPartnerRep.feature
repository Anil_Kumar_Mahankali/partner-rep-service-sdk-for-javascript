Feature: Add Partner Rep
  Adds a partner rep

  Background:
    Given an addPartnerRepReq consists of:
      | attribute    | validation | type   |
      | firstName    | required   | string |
      | lastName     | required   | string |
      | emailAddress | required   | string |
      | accountId    | required   | string |

  Scenario Template: Success
    Given I provide an accessToken identifying me as <identity>
    And provide a valid addPartnerRepReq
    When I execute addPartnerRep
    Then a partnerRep is added to the partner-rep-service with the provided attributes
    And its id is returned

    Examples:
      | identity      |
      | a partner rep |
      | an app        |