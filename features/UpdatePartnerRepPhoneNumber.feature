Feature: Update Partner Rep Phone Number
  Updates the phone number of the partner rep with the provided id

  Scenario: Success
    Given I provide an accessToken identifying me as the partnerRep with id partnerRepId
    And I provide a valid phoneNumber
    When I execute updatePartnerRepPhoneNumber
    Then my phoneNumber is updated in the partner-rep-service