Feature: Get Partner Rep With Id
  Gets the partner rep with the provided id

  Scenario Outline: Success
    Given I provide the partnerRepId of a partnerRep in the partner-rep-service
    And provide an accessToken identifying me as <identity>
    When I execute getPartnerRepWithId
    And the partnerRep is returned
    Examples:
      | identity                                            |
      | a partner rep with the same accountId as partnerRep |
      | an app                                              |
