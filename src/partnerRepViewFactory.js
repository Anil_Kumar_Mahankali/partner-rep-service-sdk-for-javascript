import PartnerRepBankInfoUpdate from './partnerRepBankInfoUpdate';
import PartnerRepView from './partnerRepView';
import PartnerRepW9Update from './partnerRepW9Update';
import {PostalAddressFactory} from 'postal-object-model';

export default class PartnerRepViewFactory {

    /**
     * @param {object} data
     * @returns {PartnerRepView}
     */
    static construct(data):PartnerRepView {

        const id = data.id;

        const firstName = data.firstName;

        const lastName = data.lastName;

        const emailAddress = data.emailAddress;

        const accountId = data.accountId;

        const groupId = data.groupId;

        let postalAddress;
        if (data.postalAddress) {
            postalAddress = PostalAddressFactory.construct(data.postalAddress);
        }

        const phoneNumber = data.phoneNumber;

        const sapVendorNumber = data.sapVendorNumber;

        const sapAccountNumber = data.sapAccountNumber;

        return new PartnerRepView(
            id,
            firstName,
            lastName,
            emailAddress,
            accountId,
            postalAddress,
            phoneNumber,
            sapVendorNumber,
            groupId,
            sapAccountNumber
        );

    }

}
