export default class GetPartnerRepW9UpdateUrlReq {

    _partnerRepId:number;

    _returnUrl:string;

    /**
     * @param {number} partnerRepId
     * @param {string} returnUrl
     */
    constructor(partnerRepId:number,
                returnUrl:string) {

        if (!partnerRepId) {
            throw new TypeError('partnerRepId required');
        }
        this._partnerRepId = partnerRepId;

        if (!returnUrl) {
            throw new TypeError('returnUrl required');
        }
        this._returnUrl = returnUrl;

    }


    get partnerRepId():number {
        return this._partnerRepId;
    }

    get returnUrl():string {
        return this._returnUrl;
    }

    toJSON() {
        return {
            partnerRepId: this._partnerRepId,
            returnUrl: this._returnUrl
        };
    }
}