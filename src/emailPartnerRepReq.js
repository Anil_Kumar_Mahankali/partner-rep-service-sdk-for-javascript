/**
 * @class {EmailPartnerRepReq}
 */
export default class EmailPartnerRepReq {

    _firstName:string;

    _lastName:string;

    _userId:string;

    _emailAddress:string;

    _sapAccountNumber:string;

    _groupId:string;

    /**
     * @param {string} firstName
     * @param {string} lastName
     * @param {string} userId
     * @param {string} emailAddress
     * @param {string} sapAccountNumber
     * @param {string} groupId
     */
    constructor(firstName:string,
                lastName:string,
                userId:string,
                emailAddress:string,
                sapAccountNumber:string,
                groupId:string
    ) {

        if(!firstName){
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if(!lastName){
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if (!userId) {
            throw new TypeError('userId required');
        }
        this._userId = userId;

        if (!emailAddress) {
            throw new TypeError('emailAddress required');
        }
        this._emailAddress = emailAddress;

        if (!sapAccountNumber) {
            throw new TypeError('sapAccountNumber required');
        }
        this._sapAccountNumber = sapAccountNumber;

        if (!groupId) {
            throw new TypeError('groupId required');
        }
        this._groupId = groupId;

    }

    /**
     * @returns {string}
     */
    get firstName():string {
        return this._firstName;
    }

    /**
     * @returns {string}
     */
    get lastName():string {
        return this._lastName;
    }

    /**
     * @returns {string}
     */
    get userId():string {
        return this._userId;
    }

    /**
     * @returns {string}
     */
    get emailAddress():string {
        return this._emailAddress;
    }

    /**
     * @returns {string}
     */
    get sapAccountNumber():string {
        return this._sapAccountNumber;
    }

    /**
     * @returns {string}
     */
    get groupId():string {
        return this._groupId;
    }

    toJSON() {
        return {
            firstName:this._firstName,
            lastName:this._lastName,
            userId: this._userId,
            emailAddress: this._emailAddress,
            sapAccountNumber: this._sapAccountNumber,
            groupId: this._groupId
        }
    }
};
